package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout
import scala.collection.mutable
import rx.lang.scala.Observable
import scala.language.postfixOps
import java.util.concurrent.TimeUnit

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case class Repersist(id: Long)
  case class CheckFailed(origSender: ActorRef, message: OperationFailed)

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  var persistence = context actorOf persistenceProps
  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  var expectedSeq = 0
  val unacknowledged = new mutable.LinkedHashMap[Long, Map[ActorRef, (ActorRef, Replicate)]]
  var unpersisted = Map.empty[Long, (ActorRef, Persist)]

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* TODO Behavior for the leader role. */
  val leader: Receive = {
    case Replicas(replicaSet) =>
      (replicaSet - self) foreach { secondary: ActorRef =>
        if (!(secondaries contains secondary)) {
          val replicator = context.actorOf(Replicator props secondary)
          secondaries += (secondary -> replicator)
          var id = 0L
          kv foreach { case (key: String, value: String) =>
            val message = Replicate(key, Some(value), id)
            replicateToOne(self, message)(replicator)
            id += 1
          }
        }
      }

      var dropped = Set.empty[ActorRef]
      secondaries foreach { case (secondary: ActorRef, replicator: ActorRef) =>
        if (!(replicaSet contains secondary)) {
          context stop replicator
          secondaries -= secondary
          dropped += replicator
        }
      }

      unacknowledged foreach { case (id: Long, replicatorMap: Map[ActorRef, (ActorRef, Replicate)]) =>
        dropped foreach { replicator: ActorRef =>
          val maybeSenderMessage = deregisterAcknowledgement(id, replicator)
          if (maybeSenderMessage.isDefined) {
            ackIfNotPending(maybeSenderMessage.get._1, OperationAck(id))
          }
        }
      }

      replicators = secondaries.values.toSet

    case Insert(key, value, id) =>
      kv += (key -> value)
      persist(sender, Persist(key, Some(value), id))
      val message = Replicate(key, Some(value), id)
      replicate(sender, message)
    case Remove(key, id) =>
      kv -= key
      persist(sender, Persist(key, None, id))
      val message = Replicate(key, None, id)
      replicate(sender, message)
    case Get(key, id) =>
      lookup(key, id, sender)
    case Replicated(key, id) =>
      val maybeSenderMessage = deregisterAcknowledgement(id, sender)
      if (maybeSenderMessage.isDefined) {
        ackIfNotPending(maybeSenderMessage.get._1, OperationAck(id))
      }
    case Persisted(key, id) =>
      val origSender = unpersisted(id)._1
      unpersisted -= id
      ackIfNotPending(origSender, OperationAck(id))
    case Repersist(id) =>
      repersist(id)
    case CheckFailed(origSender, message) =>
      signalIfPending(origSender, message)
    case OperationAck(id) =>
    case OperationFailed(id) =>
  }

  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    case Snapshot(key, valueOption, seq) if seq < expectedSeq =>
      sender ! SnapshotAck(key, seq)
    case Snapshot(key, valueOption, seq) if seq > expectedSeq =>
      () // ignored
    case Snapshot(key, valueOption, seq) if valueOption.isEmpty =>
      kv -= key
      persist(sender, Persist(key, valueOption, seq))
    case Snapshot(key, valueOption, seq) =>
      kv += (key -> valueOption.get)
      persist(sender, Persist(key, valueOption, seq))
    case Get(key, id) =>
      lookup(key, id, sender)
    case Persisted(key, seq) =>
      val origSender = unpersisted(seq)._1
      unpersisted -= seq
      ackSatisfiedSnapshot(origSender, SnapshotAck(key, seq))
    case Repersist(id) =>
      repersist(id)
  }

  def lookup(key: String, id: Long, sender: ActorRef) {
    val maybeValue = kv get key
    sender ! GetResult(key, maybeValue, id)
  }

  def replicateToOne(sender: ActorRef, message: Replicate)(replicator: ActorRef) {
    if (unacknowledged contains message.id) {
      unacknowledged(message.id) += replicator -> (sender, message)
    } else {
      unacknowledged += message.id -> Map(replicator -> (sender, message))
    }
    replicator ! message
  }

  def replicate(sender: ActorRef, message: Replicate) {
    replicators foreach replicateToOne(sender, message)
    Observable.interval(1 second).take(1) subscribe { sequence: Long =>
      self ! CheckFailed(sender, OperationFailed(message.id))
    }
  }

  def signalIfPending(sender: ActorRef, message: OperationFailed) {
    if ((unacknowledged contains message.id) || (unpersisted contains message.id)) {
      sender ! message
      unacknowledged -= message.id
      unpersisted -= message.id
    }
  }

  def ackIfNotPending(sender: ActorRef, message: OperationAck) {
    if (!(unacknowledged contains message.id) && !(unpersisted contains message.id)) {
      sender ! message
    }
  }

  def deregisterAcknowledgement(id: Long, replicator: ActorRef): Option[(ActorRef, Replicate)] = {
    val messageOption: Option[(ActorRef, Replicate)] = for (
      replicatorMap <- unacknowledged get id;
      senderMessagePair <- replicatorMap get replicator
    ) yield senderMessagePair
    if (messageOption.isDefined) {
      unacknowledged(id) -= replicator
      if (unacknowledged(id).isEmpty) {
        unacknowledged -= id
      }
      ()
    }
    messageOption
  }

  def persist(origSender: ActorRef, message: Persist) {
    unpersisted += message.id -> (origSender, message)
    persistence ! message
    Observable.interval(100 milliseconds).take(1) subscribe { id: Long =>
      self ! Repersist(message.id)
    }
  }

  def ackSatisfiedSnapshot(origSender: ActorRef, message: SnapshotAck) {
    if (message.seq == expectedSeq) {
      expectedSeq += 1
    }
    origSender ! message
  }

  def repersist(id: Long) {
    if (unpersisted contains id) {
      unpersisted(id) match {
        case (origSender: ActorRef, message: Persist) => persist(origSender, message)
      }
    }
  }

  arbiter ! Join
}
