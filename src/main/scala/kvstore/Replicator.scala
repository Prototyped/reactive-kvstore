package kvstore

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorRef
import scala.concurrent.duration._
import scala.collection.immutable.TreeMap
import scala.language.postfixOps
import rx.lang.scala.Observable
import java.util.concurrent.TimeUnit

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  case object Resend

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor {
  import Replicator._
  import Replica._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = TreeMap.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }
  
  /* TODO Behavior for the Replicator. */
  def receive: Receive = {
    case request @ Replicate(key, valueOption, id) =>
      val seq = nextSeq
      acks += (seq -> (sender, request))
      replica ! Snapshot(key, valueOption, seq)
    case SnapshotAck(key, seq) =>
      val (origSender: ActorRef, request: Replicate) = acks(seq)
      acks -= seq
      request match {
        case Replicate(_, _, id) =>
          origSender ! Replicated(key, id)
      }
    case Resend =>
      acks foreach {
        case (seq: Long, (origSender: ActorRef, request: Replicate)) =>
          request match {
            case Replicate(key, valueOption, id) =>
              replica ! Snapshot(key, valueOption, seq)
          }
      }
  }

  Observable.interval(100 milliseconds) subscribe { value: Long =>
    self ! Resend
  }
}
